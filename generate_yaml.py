
import sys
from ruamel.yaml import YAML

def writeyml(params):
    # params = "wntx, tc13.0.0.0, 7002"
    inp = """\
    # build yaml
    build wntx:
      tags: 
        - """ + params + """
      script: 
        - python -m pip install
        - python -m pip install conan-package-tools
        - python build.py
    """

    yaml = YAML()
    code = yaml.load(inp)
    path = 'build.yml'
    days_file = open(path,'w')

    yaml.dump(code, days_file)
    
    return

if __name__ == "__main__":
    try:
        params = " ".join(sys.argv[1:])
        writeyml(params)
    except Exception as e:
            print(e)
            sys.exit(1)