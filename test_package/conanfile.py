# test_package/conanfile.py - file that decides what commands are to be run to test the package
#
#   The test() function specifies what commands need to be run to verify each built 
#       package.  In this example, simply running the executable is enough to verify
#       that the package is working as desginated.

from conans import ConanFile, tools

class HelloTestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
        self.copy('*.so*', dst='bin', src='lib')

    def test(self):
        self.run("hello.exe")

