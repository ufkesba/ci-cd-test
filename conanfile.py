# conanfile.py - file that decides what commands are to be run to build the package
# 
#  Important Functions: 
#    configure() - "If the package options and settings are related, and you want 
#       to configure either, you can do so in the configure() and config_options() 
#       methods."
#
#    build() - "This method is used to build the source code of the recipe using 
#       the desired commands. You can use your command line tools to invoke your 
#       build system or any of the build helpers provided with Conan."
#
#    package() - "The actual creation of the package, once that it is built, is 
#       done in the package() method. Using the self.copy() method, artifacts are 
#       copied from the build folder to the package folder."
#
#    package_info() - "Each package has to specify certain build information for 
#       its consumers. This can be done in the cpp_info attribute within the 
#       package_info() method."
#
#    For more info, refer to: https://docs.conan.io/en/latest/reference/conanfile/methods.html


from conans import ConanFile, tools
from conans.errors import ConanInvalidConfiguration
import os

class HelloConan(ConanFile):
    name = "FOSS_Template"
    version = "0.2"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Hello here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}

    # indicates to Conan to copy all the files from the local src folder into the package recipe.
    exports_sources = ["hello/*"]

    def configure(self):
        if self.settings.os != "Windows":
            raise ConanInvalidConfiguration("Only Windows supported")
        if self.settings.arch not in ("x86", "x86_64"):
            raise ConanInvalidConfiguration("Unsupported architecture")

    def _build_msvc(self):
        with tools.vcvars(self.settings):
            with tools.chdir(os.path.join("hello")):
                self.run("nmake /f Makefile.vc")

    def build(self):
        '''
        Run _build_msvc if the chosen compiler version is Visual Studio
        '''
        if self.settings.compiler == "Visual Studio":
            self._build_msvc()

    def package(self):
        '''
        Packaging the desired files generated in the build stage
        '''
        self.copy("*.h", dst="include", src="hello")
        self.copy("*hello.lib", dst="lib"+ str(self.settings.arch), keep_path=False)
        self.copy("*.exe", dst="bin_"+ str(self.settings.arch), keep_path=False)
        self.copy("*.dll", dst="bin_"+ str(self.settings.arch), keep_path=False)
        self.copy("*.so", dst="lib_"+ str(self.settings.arch), keep_path=False)
        self.copy("*.dylib", dst="lib_"+ str(self.settings.arch), keep_path=False)
        self.copy("*.a", dst="lib_" + str(self.settings.arch), keep_path=False)

    def package_info(self):
        '''
        Allows hello.exe to be used in test.  Appends libs and adds directories to PATH
        '''    
        self.cpp_info.libs = ["hello"]
        bin_path = os.path.join(self.package_folder, "bin_"+ str(self.settings.arch))
        self.output.info('Appending PATH environment variable: %s' % bin_path)
        self.env_info.path.append(bin_path)
        

