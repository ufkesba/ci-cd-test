// Simple Hello World program written for:
//   https://gitlab.industrysoftware.automation.siemens.com/teamcenter_third-party_components/foss_template

#pragma once

#ifdef WIN32
  #define HELLO_EXPORT __declspec(dllexport) 
#else
  #define HELLO_EXPORT  
#endif

HELLO_EXPORT void hello();