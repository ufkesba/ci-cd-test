// Simple Hello World program written for:
//   https://gitlab.industrysoftware.automation.siemens.com/teamcenter_third-party_components/foss_template

#include <iostream>

#include "hello.h"


int main() {
    #ifdef NDEBUG
    std::cout << "Hello World Release!" <<std::endl;
    #else
    std::cout << "Hello World Debug!" <<std::endl;
    #endif
}
