# C++ Hello World

This is a "Hello World" library (``hello``) and application (``greet``)
used for demo purposes on examples about Conan C/C++ Package Manager.

Forked and modified to use nmake as opposed to cmake from https://github.com/conan-io/hello

## License

[MIT](LICENSE)