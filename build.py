# Build + Test file for the FOSS_Template repository.  Runs the builds designated in conanfile.py, 
#   Then, the tests specified under test_package/conanfile.py will be executed for each build.

import os, sys, json
import platform
from cpt.packager import ConanMultiPackager
import shutil

def recursive_overwrite(src, dest, ignore=None):
    ''' Helper function for copying atifacts '''
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            os.makedirs(dest)
        files = os.listdir(src)
        if ignore is not None:
            ignored = ignore(src, files)
        else:
            ignored = set()
        for f in files:
            if f not in ignored:
                recursive_overwrite(os.path.join(src, f), 
                                    os.path.join(dest, f), 
                                    ignore)
    else:
        shutil.copyfile(src, dest)

def copy_artifacts():
    '''
    Copy artifacts to the local_location based on the Conan Package Tools Summary file, "cpt_summary_file.json".  
    The summary file shows where the packages were written to on the local machine.  
    We use this function to combine those files together to form the "Toolbox Ready" kit of the FOSS Component
    '''
    local_location = os.path.join(os.getcwd(),"ARTIFACTS")
    try:
        with open('cpt_summary_file.json', 'r') as f:
            packages_dict = json.load(f)
            for package in packages_dict:
                package_loc = package['package']['installed'][0]['packages'][0]['cpp_info']['rootpath']
                print("Copying package: " + package_loc)
                recursive_overwrite(package_loc, local_location)
            print("\nArtifacts located at: " + local_location + "\n")

    except Exception as e:
        print (e)

###############################################################################################
# Main function to perform the build/test of the 3rd party component. 
#
#   The builder builds on top of the Conan C++ Packager.  
#       https://docs.conan.io/en/latest/introduction.html
#
#   The builder is used to automate the creation of conan packages for different configurations.
#       https://github.com/conan-io/conan-package-tools
#
#   The builder performs a build/test/package of each of the provided configurations.  In this 
#       example, a build is needed for both x86 + x64, on Visual Studio 2017 (vc15).  The 
#       configurations are determined by each builder.add(), and then by running builder.run(),
#       the script runs a build, packages up the binaries, and then runs a test on those built
#       binaries
#       
###############################################################################################
if __name__ == "__main__":
    try:
        params = " ".join(sys.argv[1:])
        builder = ConanMultiPackager()
        if platform.system() == "Windows":
            builder.add(settings={"arch": "x86_64", "compiler": "Visual Studio","compiler.version": "15"},
                        options={}, env_vars={}, build_requires={})
            builder.add(settings={"arch": "x86", "compiler": "Visual Studio","compiler.version": "15"},
                        options={}, env_vars={}, build_requires={})
            builder.run(summary_file='cpt_summary_file.json')


        else:
            print("Unrecognized Platform. Build script is only set up for Windows")
            sys.exit(1)
            pass
    except Exception as e:
        print(e)
        sys.exit(1)
    
    copy_artifacts()
    sys.exit(0)